/*
 * Implement bukkit plugin interface
 */

package xeth.flags

import java.io.File
import kotlin.math.ceil
import kotlin.math.min
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.InventoryHolder
import org.bukkit.inventory.ItemStack
import org.bukkit.entity.Player
import net.kyori.adventure.text.Component


/**
 * Helper for sending messages to player.
 */
public object Message {

    public val PREFIX = "[Flags]"
    public val COL_MSG = ChatColor.DARK_GREEN
    public val COL_ERROR = ChatColor.RED

    // print generic message to chat
    public fun print(sender: CommandSender?, s: String) {
        if ( sender === null ) {
            System.out.println("${PREFIX} Message called with null sender: ${s}")
            return
        }

        val msg = Component.text("${COL_MSG}${s}")
        sender.sendMessage(msg)
    }

    // print error message to chat
    public fun error(sender: CommandSender?, s: String) {
        if ( sender === null ) {
            System.out.println("${PREFIX} Message called with null sender: ${s}")
            return
        }

        val msg = Component.text("${COL_ERROR}${s}")
        sender.sendMessage(msg)
    }
}


public class FlagCommand(val plugin: FlagPlugin) : CommandExecutor, TabCompleter {

    public val subcommands = listOf("help", "reload", "get")

    override fun onCommand(sender: CommandSender, cmd: Command, commandLabel: String, args: Array<String>): Boolean {
        
        val player = if ( sender is Player ) sender else null
    
        // no args, open flags gui page 1
        if ( args.size == 0 ) {
            if ( player !== null ) {
                plugin.createFlagGui(player, 0)
            }
            else {
                this.printHelp(sender)
            }
            return true
        }

        // parse subcommand
        val subcmd = args[0].lowercase()
        when ( subcmd ) {
            "help" -> printHelp(sender)
            "reload" -> reload(sender)
            "get" -> {
                if ( player !== null ) {
                    if ( args.size < 2 ) {
                        Message.error(player, "Usage is /flag get [name]")
                        return true
                    }
                    val flagName = args[1].lowercase()
                    if ( !plugin.giveFlagToPlayer(player, flagName) ) {
                        Message.error(player, "Invalid flag name: ${flagName}")
                    }
                }
                else {
                    Message.error(sender, "Must be player ingame to use /flag get [name]")
                }
            }
            // open a flag page
            else -> {
                if ( player !== null ) {
                    val page: Int = try {
                        subcmd.toInt()
                    }
                    catch ( e: Exception ) {
                        Message.error(sender, "Invalid flag page number")
                        return true
                    }

                    if ( page < 1 || page > plugin.flagPages ) {
                        Message.error(sender, "Invalid flag page number: ${page}, must be between 1 and ${plugin.flagPages}")
                        return true
                    }

                    plugin.createFlagGui(player, page-1)
                }
                else {
                    Message.error(sender, "Must be player ingame to use /flag #")
                }
            }
        }

        return true
    }

    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<String>): List<String> {
        return when ( args.size ) {
            1 -> this.subcommands
            2 -> if ( args[0] == "get" ) plugin.flagNames else listOf()
            else -> listOf()
        }
    }

    private fun printHelp(sender: CommandSender?) {
        Message.print(sender, "[Flags] for Mineman 1.18.2")
        Message.print(sender, "/flag help${ChatColor.WHITE}: help")
        Message.print(sender, "/flag reload${ChatColor.WHITE}: reload config")
        Message.print(sender, "/flag${ChatColor.WHITE}: open flag gui page 1")
        Message.print(sender, "/flag #${ChatColor.WHITE}: open flag gui page #, where 1 <= # <= ${plugin.flagPages}")
        Message.print(sender, "/flag get [name]${ChatColor.WHITE}: get specific flag from name (if it exists)")
        return
    }

    private fun reload(sender: CommandSender?) {
        val player = if ( sender is Player ) sender else null
        if ( player === null || player.isOp() ) {
            plugin.loadConfig()
            Message.print(sender, "[Flags] reloaded")
        }
        else {
            Message.error(sender, "[Flags] Only operators can reload")
        }
    }
}

/**
 * Helper to create flag item stack.
 */
private fun createFlagItem(flagMat: Material, name: String, customModelData: Int): ItemStack {
    val item = ItemStack(flagMat, 1)
    val meta = item.getItemMeta()!!
    meta.displayName(Component.text(name))
    meta.setCustomModelData(customModelData)
    item.setItemMeta(meta)
    return item
}

/**
 * Flag item data wrapper.
 */
private data class FlagItem(
    val name: String,
    val customModelData: Int,
    val material: Material,
) {
    val item: ItemStack = createFlagItem(material, name, customModelData)
}


public class FlagPlugin : JavaPlugin() {

    // damage value and period between damage ticks
    public var flagMat: Material = Material.MUSIC_DISC_PIGSTEP
        private set

    // list of flag items with pre-generated itemstacks
    private var flags: List<FlagItem> = listOf()

    // flag names string keys for /flag get [name] command
    public var flagNames: List<String> = listOf()
        private set

    // map from flag name (above) -> customModelIndex
    private var flagNameToItem: Map<String, FlagItem> = mapOf()

    // number of flag item pages
    private val flagsPerPage = 54 // number of inventory slots per page
    public var flagPages: Int = 0
        private set
    
    /**
     * Load config, update settings
     * Returns number of flags loaded
     */
    public fun loadConfig(): Int {
        val flags: ArrayList<FlagItem> = arrayListOf()
        val flagNames: ArrayList<String> = arrayListOf()
        val flagNameToItem: HashMap<String, FlagItem> = hashMapOf()

        // get config file
        val configFile = File(this.getDataFolder().getPath(), "config.yml")
        if ( !configFile.exists() ) {
            this.getLogger().info("No config found: generating default config.yml")
            this.saveDefaultConfig()
        }
        else {
            val config = YamlConfiguration.loadConfiguration(configFile)
            this.flagMat = config.getString("flagMat")?.let{ s -> Material.matchMaterial(s) } ?: this.flagMat

            // parse list of "- flagname: index"
            val configFlagsList = config.getConfigurationSection("flags")
            if ( configFlagsList !== null ) {
                val configFlagNames = configFlagsList.getKeys(false)
                for ( name in configFlagNames ) {
                    val flagCustomModelIndex = configFlagsList.getInt(name, 0)
                    val flag = FlagItem(name, flagCustomModelIndex, this.flagMat)
                    val flagKey = name.lowercase().replace(" ", "_")
                    
                    flags.add(flag)
                    flagNames.add(flagKey)
                    flagNameToItem.put(flagKey, flag)
                }
            }
        }

        this.flags = flags
        this.flagNames = flagNames
        this.flagNameToItem = flagNameToItem
        this.flagPages = ceil(flags.size.toDouble() / flagsPerPage.toDouble()).toInt() // # of pages

        return flags.size
    }

    /**
     * Inventory containing flag items. Input takes the flag "page" in order
     * to paginate the list of flags. Input "page" should be 0-indexed,
     * page must be [0, flagPages-1]. If page is < 0 or >= flagPages, then
     * the inventory will be empty.
     */
    private class FlagGui(
        val plugin: FlagPlugin,
        val page: Int,
    ): InventoryHolder {
        val inv: Inventory = Bukkit.createInventory(this, plugin.flagsPerPage, Component.text("Flags (Page ${page+1}/${plugin.flagPages})"))
        
        override public fun getInventory(): Inventory {
            // if page within pagination bounds, fill inventory with flag items
            if ( page >= 0 && page < plugin.flagPages ) {
                var nStart = page * plugin.flagsPerPage
                var nEnd = min(nStart + plugin.flagsPerPage, plugin.flags.size)
                for ( n in nStart until nEnd ) {
                    this.inv.addItem(plugin.flags[n].item.clone())
                }
            }

            return this.inv
        }
    }

    /**
     * Create flag gui for player. Input takes the flag "page" in order
     * to paginate the list of flags. Input "page" should be 0-indexed,
     * page must be [0, flagPages-1].
     */
    public fun createFlagGui(player: Player, page: Int) {
        player.openInventory(FlagGui(this, page).getInventory())
    }

    /**
     * Give flag to player, return true if successful
     */
    public fun giveFlagToPlayer(player: Player, key: String): Boolean {
        val flag = this.flagNameToItem.get(key)
        if ( flag !== null ) {
            player.getInventory().addItem(flag.item.clone())
            return true
        }
        return false
    }

    /**
     * Plugin enable. Load config, register commands.
     */
    override fun onEnable() {
        // measure load time
        val timeStart = System.currentTimeMillis()

        val logger = this.getLogger()

        // load flags
        val numFlags = this.loadConfig()
        logger.info("Loaded ${numFlags} Flags")

        // register commands
        this.getCommand("flag")?.setExecutor(FlagCommand(this))

        // print load time
        val timeEnd = System.currentTimeMillis()
        val timeLoad = timeEnd - timeStart
        logger.info("Enabled in ${timeLoad}ms")

        // print success message
        logger.info("now this is epic")
    }

    /**
     * Plugin disable.
     */
    override fun onDisable() {
        logger.info("wtf i hate xeth now")
    }
}
